import java.io.*;

/**
 * @author hahah
 * @date 2018/10/13
 */
public class Command {
    String[] command;
    boolean isC;
    boolean isW;
    boolean isL;
    boolean isS;
    String fileName;
    String outFile;
    int charNum;
    int wordNum;
    int lineNum;

    public Command(String[] command) {
        try {
            initCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initCommand(String[] command) throws Exception {
        this.command=command;
        this.isC=false;
        this.isW=false;
        this.isL=false;
        this.outFile="result.txt";

        if("-o".equals(command[0]))
        {
            throw new Exception("命令参数出错！");
        }
        for(int i=0;i<command.length;i++) {
            if ("-c".equals(command[i])) {
                this.isC = true;
            }
            if ("-w".equals(command[i])) {
                this.isW = true;
            }
            if ("-l".equals(command[i])) {
                this.isL = true;
            }
            if ("-o".equals(command[i])) {
                char ch = command[i+1].charAt(0);
                if (ch != '-') {
                    this.outFile=command[i+1];
                    i++;
                } else {
                    throw new Exception("命令参数出错！");
                }
            }
            if(i!=0 && command[i].charAt(0)!='-') {
                if ("-c".equals(command[i-1])||"-w".equals(command[i-1])||"-l".equals(command[i-1])){
                    this.fileName=command[i];
                }
            }else {
                if (command[0].charAt(0)!='-'){
                    throw new Exception("命令参数出错！");
                }
            }

        }
    }

    public void charNum(String fileName)
    {
        File file = new File(fileName);
        BufferedReader br=null;
        BufferedWriter bw=null;
        try{
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                char[] des = temp.toCharArray();
                charNum+=des.length;
            }
            System.out.println(this.fileName+",字符数："+charNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(this.fileName+",字符数："+charNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
    }
    public void wordNum(String fileName)
    {
        File file = new File(fileName);
        BufferedReader br=null;
        BufferedWriter bw=null;
        try{
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                String[] des = temp.split(" |,");
                for (String de : des) {
                    if(!"".equals(de)){
                        wordNum++;
                    }
                }
            }
            System.out.println(this.fileName+",单词数："+wordNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(this.fileName+",单词数："+wordNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
    }
    public void lineNum(String fileName)
    {
        File file = new File(fileName);
        BufferedReader br=null;
        BufferedWriter bw=null;
        try{
            br = new BufferedReader(new FileReader(file));
            while ((br.readLine()) != null) {
                lineNum++;
            }
            System.out.println(this.fileName+",行数："+lineNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(this.fileName+",行数："+lineNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            System.out.println("文件不存在");
        }
    }


    public void run() {
        if (this.isC){
            charNum(this.fileName);
        }
        if (this.isW){
            wordNum(this.fileName);
        }
        if (this.isL){
            lineNum(this.fileName);
        }
    }
}

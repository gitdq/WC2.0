/**
 * @author hahah
 * @date 2018/10/13
 */
public class Client {
    public static void main(String[] args) {
        Command command = new Command(args);
        command.run();
    }
}

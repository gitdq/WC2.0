import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hahah
 * @date 2018/10/13
 */
public class Command {
    private String[] command;
    private boolean isC;
    private boolean isW;
    private boolean isL;
    private boolean isS;
    private boolean isA;
    private boolean isE;
    private String fileName;
    private String outFile;
    private String stopList;
    private int charNum;
    private int wordNum;
    private int lineNum;
    private int codeNum;
    private int notesNum;
    private int spaceNum;
    public Command(String[] command) {
        try {
            initCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initCommand(String[] command) throws Exception {
        this.command=command;
        this.isC=false;
        this.isW=false;
        this.isL=false;
        this.isA=false;
        this.isE=false;
        this.isS=false;
        this.outFile="result.txt";

        if("-o".equals(command[0]) || "-e".equals(command[0])) {
            throw new Exception("命令参数出错！");
        }
        for(int i=0;i<command.length;i++) {
            if ("-c".equals(command[i])) {
                this.isC = true;
            }
            if ("-w".equals(command[i])) {
                this.isW = true;
            }
            if ("-l".equals(command[i])) {
                this.isL = true;
            }
            if ("-a".equals(command[i])) {
                this.isA = true;
            }
            if ("-s".equals(command[i])) {
                this.isS = true;
            }
            if ("-o".equals(command[i])) {
                char ch = command[i+1].charAt(0);
                if (ch != '-') {
                    this.outFile=command[i+1];
                    i++;
                } else {
                    throw new Exception("命令参数出错！");
                }
            }
            if ("-e".equals(command[i])) {
                char ch = command[i+1].charAt(0);
                if (ch != '-') {
                    this.isE=true;
                    this.stopList=command[i+1];
                    i++;
                } else {
                    throw new Exception("命令参数出错！");
                }
            }
            if(i!=0 && command[i].charAt(0)!='-') {
                if ("-c".equals(command[i-1])||"-w".equals(command[i-1])||"-l".equals(command[i-1])
                        ||"-a".equals(command[i-1])||"-s".equals(command[i-1])){
                    this.fileName=command[i];
                }
            }else {
                if (command[0].charAt(0)!='-'){
                    throw new Exception("命令参数出错！");
                }
            }

        }
    }

    private void charNum(File file)
    {
        try{
            this.charNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                char[] des = temp.toCharArray();
                charNum+=des.length;
            }
            System.out.println(file.getName()+",字符数："+charNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",字符数："+charNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
    }
    private void wordNum(File file)
    {
        try{
            this.wordNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            if(this.isE){
                while ((temp = br.readLine()) != null) {
                    String[] des = temp.split(" |,");
                    for (String de : des) {
                        if(!"".equals(de)&&!inStopList(de)){
                            wordNum++;
                        }
                    }
                }
            }else {
                while ((temp = br.readLine()) != null) {
                    String[] des = temp.split(" |,");
                    for (String de : des) {
                        if(!"".equals(de)){
                            wordNum++;
                        }
                    }
                }
            }
            System.out.println(file.getName()+",单词数："+wordNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",单词数："+wordNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
    }

    private boolean inStopList(String word) {
        try{
            File file = new File(this.stopList);
            BufferedReader br=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp=br.readLine())!= null) {
                String[] des = temp.split(" |,");
                for (String de : des) {
                    if(!"".equals(de)){
                        if (de.equals(word)){
                            br.close();
                            return true;
                        }
                    }
                }
            }
            br.close();
        }
        catch(Exception e){
            System.out.println("文件不存在");
        }
        return false;
    }

    private void lineNum(File file)
    {
        try{
            this.lineNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            while ((br.readLine()) != null) {
                lineNum++;
            }
            System.out.println(file.getName()+",行数："+lineNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",行数："+lineNum+"。\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            System.out.println("文件不存在");
        }
    }

    private void complexStat(File file) {
        String regexNodeBegin = "\\s*/\\*.*";
        String regexNodeEnd = ".*\\*/\\s*";
        String regexNode = "\\s*/{2}.*";
        String regexSpace = "\\s*";
        try {
            notesNum=0;
            spaceNum=0;
            codeNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                if (temp.matches(regexNodeBegin)) {
                    do {
                        notesNum++;
                        temp = br.readLine();
                    } while (!temp.matches(regexNodeEnd));
                    notesNum++;
                } else if (temp.matches(regexSpace)) {
                    spaceNum++;
                } else if (temp.matches(regexNode)) {
                        notesNum++;
                } else{
                    codeNum++;
                }
            }
            System.out.println(file.getName()+",代码行/空行/注释行：" + codeNum + "/" + spaceNum + "/" + notesNum);
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",代码行/空行/注释行：" + codeNum + "/" + spaceNum + "/" + notesNum+"。\n");
            bw.close();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("文件不存在");
        }
    }
    private void recursiveFiles(String fileName){
        File dir = new File(".");
        List<File> files = getFile(dir);
        for (File file : files) {
            if (this.isC) {
                charNum(file);
            }
            if (this.isW) {
                wordNum(file);
            }
            if (this.isL) {
                lineNum(file);
            }
            if (this.isA) {
                complexStat(file);
            }
        }
    }
    private List<File> getFile(File dir) {
        List<File> files = new ArrayList<>();
        File[] subs = dir.listFiles();
        for (File file : subs) {
            if (file.isFile() && file.getName().endsWith(this.fileName.substring(1))) {
                files.add(file);
            } else if (file.isDirectory()) {
                files.addAll(getFile(file));
            }
        }
        return files;
    }

    public void run() {
        if (this.isS){
            recursiveFiles(this.fileName);
        }else {
            if (this.isC) {
                charNum(new File(this.fileName));
            }
            if (this.isW) {
                wordNum(new File(this.fileName));
            }
            if (this.isL) {
                lineNum(new File(this.fileName));
            }
            if (this.isA) {
                complexStat(new File(this.fileName));
            }
        }
    }
}
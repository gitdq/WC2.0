import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;

public class WcGUI {

    public void showUI() {
        Command cm = new Command();

        JFrame f = new JFrame("WordCount");
        f.setLayout(null);
        JFileChooser fc = new JFileChooser();

        JButton bOpenDir = new JButton("打开文件或文件夹并写入");
        bOpenDir.setBounds(220,15,180,30);

        JButton bResult = new JButton("修改写入文件");
        bResult.setBounds(50,15,140,30);

        JCheckBox cb = new JCheckBox("使用停用词表");
        cb.setBounds(430,15,150,30);

        JTextArea ja = new JTextArea();
        ja.setFont(new Font("黑体",Font.BOLD,15));
        ja.setEditable(false);

        JScrollPane jsp=new JScrollPane(ja);
        jsp.setBounds(50,50,600,400);

        f.add(bOpenDir);
        f.add(bResult);
        f.add(cb);
        f.add(jsp);


        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(700, 500);
        f.setLocationRelativeTo(null);
        f.setVisible(true);

        bOpenDir.addActionListener(e -> {
            ja.setText("");
            FileSystemView fsv = FileSystemView.getFileSystemView();
            //设置当前路径为desktop
            fc.setCurrentDirectory(fsv.getHomeDirectory());
            fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            if (fc.showOpenDialog(f) == JFileChooser.APPROVE_OPTION) {
                File dir = fc.getSelectedFile();
                if (dir.isDirectory()) {
                    List<File> files = cm.getFile(dir);
                    for (File file : files) {
                        ja.append(cm.charNum(file) + cm.wordNum(file)
                                + cm.lineNum(file) + cm.complexStat(file) + "\n\n" );
                        ja.setCaretPosition(ja.getText().length());
                    }
                } else if (dir.isFile()) {
                    ja.append(cm.charNum(dir) + cm.wordNum(dir)
                            + cm.lineNum(dir) + cm.complexStat(dir) + "\n\n" );
                    ja.setCaretPosition(ja.getText().length());
                }
            }
        });

        bResult.addActionListener(e -> {
            FileSystemView fsv=FileSystemView.getFileSystemView();
            //设置当前路径为desktop
            fc.setCurrentDirectory(fsv.getHomeDirectory());
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int returnVal = fc.showOpenDialog(f);
            File file = fc.getSelectedFile();
            String selectedFilePath = file.getAbsolutePath();
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                cm.setOutFile(selectedFilePath);
                JOptionPane.showMessageDialog(f, "所选结果写入文件：" + selectedFilePath);
            }

        });
        cb.addActionListener(e -> {
            if (cb.isSelected()){
                cm.setStopList("stopList.txt");
                cm.setE(true);
            }else {
                cm.setE(false);
            }
        });
    }
}
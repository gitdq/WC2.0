import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hahah
 * @date 2018/10/13
 */
public class Command {
    private String[] command;
    private boolean isC;
    private boolean isW;
    private boolean isL;
    private boolean isS;
    private boolean isA;
    private boolean isE;
    private boolean isX;
    private String fileName;
    private String outFile;
    private String stopList;
    private int charNum;
    private int wordNum;
    private int lineNum;
    private int codeNum;
    private int notesNum;
    private int spaceNum;
    public Command(String[] command) {
        try {
            initCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Command() {
        this.outFile="result.txt";
    }

    /**
     * 命令行参数解析方法，用于检测用到的操作参数以及相关文件名
     * @param command 传入的命令行参数
     */
    private void initCommand(String[] command) throws Exception {
        this.command=command;
        this.isC=false;
        this.isW=false;
        this.isL=false;
        this.isA=false;
        this.isE=false;
        this.isS=false;
        this.isX=false;
        this.outFile="result.txt";

        if("-o".equals(command[0]) || "-e".equals(command[0])) {
            throw new Exception("命令参数出错！");
        }
        for(int i=0;i<command.length;i++) {
            if ("-x".equals(command[i])) {
                this.isX = true;
                return;
            }
            if ("-c".equals(command[i])) {
                this.isC = true;
            }
            if ("-w".equals(command[i])) {
                this.isW = true;
            }
            if ("-l".equals(command[i])) {
                this.isL = true;
            }
            if ("-a".equals(command[i])) {
                this.isA = true;
            }
            if ("-s".equals(command[i])) {
                this.isS = true;
            }
            if ("-o".equals(command[i])) {
                char ch = command[i+1].charAt(0);
                if (ch != '-') {
                    this.outFile=command[i+1];
                    i++;
                } else {
                    throw new Exception("命令参数出错！");
                }
            }
            if ("-e".equals(command[i])) {
                char ch = command[i+1].charAt(0);
                if (ch != '-') {
                    this.isE=true;
                    this.stopList=command[i+1];
                    i++;
                } else {
                    throw new Exception("命令参数出错！");
                }
            }
            if(i!=0 && command[i].charAt(0)!='-') {
                if ("-c".equals(command[i-1])||"-w".equals(command[i-1])||"-l".equals(command[i-1])
                        ||"-a".equals(command[i-1])||"-s".equals(command[i-1])){
                    this.fileName=command[i];
                }
            }else {
                if (command[0].charAt(0)!='-'){
                    throw new Exception("命令参数出错！");
                }
            }

        }
    }

    /**
     * 字符数计算方法
     * @param file 需要计算字符数的文件
     */
    public String charNum(File file)
    {
        String str = "";
        try{
            this.charNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                char[] des = temp.toCharArray();
                //逐行读取文件内容，计算字符数并加到总数上
                charNum+=des.length;
            }
            System.out.println(file.getName()+",字符数："+charNum);
            str+=(file.getName()+",字符数："+charNum+"。\n");
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",字符数："+charNum+"。\r\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
        return str;
    }

    /**
     * 单词数计算方法
     * @param file 需要计算单词数的文件
     */
    public String wordNum(File file)
    {
        String str = "";
        try{
            this.wordNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            //如果this.isE为true，即需要使用停用词表
            if(this.isE){
                while ((temp = br.readLine()) != null) {
                    //逐行读取文件内容后，用'，'和' '将文件将行内容字符串分割为字符串数组
                    String[] des = temp.split(" |,");
                    for (String de : des) {
                        //数组中的元素，如果不是空字符串并且没有在停用词表内容就认为这是一个单词
                        if(!"".equals(de)&&!inStopList(de)){
                            wordNum++;
                        }
                    }
                }//使用停用词表模块
            }else {
                while ((temp = br.readLine()) != null) {
                    String[] des = temp.split(" |,");
                    for (String de : des) {
                        //数组中的元素，如果不是空字符串就认为这是一个单词
                        if(!"".equals(de)){
                            wordNum++;
                        }
                    }
                }//不使用停用词表模块
            }
            System.out.println(file.getName()+",单词数："+wordNum);
            str+=(file.getName()+",单词数："+wordNum+"。\n");
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",单词数："+wordNum+"。\r\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("文件不存在");
        }
        return str;
    }

    /**
     * 计算字符串是否在停用词表的方法
     * @param word 需要计算是否在停用词表内的字符串
     */
    public boolean inStopList(String word) {
        try{
            File file = new File(this.stopList);
            BufferedReader br=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp=br.readLine())!= null) {
                String[] des = temp.split(" |,");
                for (String de : des) {
                    if(!"".equals(de)){
                        //停用词表内逐个单词与传入参数对比，如果相等，说明当前参数是在停用词表内
                        if (de.equals(word)){
                            br.close();
                            return true;
                        }
                    }
                }
            }
            br.close();
        }
        catch(Exception e){
            System.out.println("文件不存在");
        }
        //如果上面所以单词对比完成都没有触发if条件，则说明不在停用词表内
        return false;
    }

    /**
     * 行数计算方法
     * @param file 需要计算行数的文件
     */
    public String lineNum(File file)
    {
        String str="";
        try{
            this.lineNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            while ((br.readLine()) != null) {
                //逐行读取文件内容，读取一次行数加一，直至将文件读取完
                lineNum++;
            }
            System.out.println(file.getName()+",行数："+lineNum);
            str+=(file.getName()+",行数："+lineNum+"。\n");
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",行数："+lineNum+"。\r\n");
            bw.close();
            br.close();
        }
        catch(Exception e){
            System.out.println("文件不存在");
        }
        return str;
    }

    /**
     * 代码行/空行/注释行 的计算方法
     * @param file 需要计算 代码行/空行/注释行 的文件
     */
    public String complexStat(File file) {
        //多行注释正则表达式
        String regexNodeBegin = "\\s*/\\*.*";
        String regexNodeEnd = ".*\\*/\\s*";
        //单行注释正则表达式
        String regexNode = "\\s*/{2}.*";
        //空行正则表达式
        String regexSpace = "\\s*";
        String str="";
        try {
            notesNum=0;
            spaceNum=0;
            codeNum=0;
            BufferedReader br=null;
            BufferedWriter bw=null;
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                if (temp.matches(regexNodeBegin)) {
                    //当前行匹配多行注释行首时执行
                    do {
                        notesNum++;
                        temp = br.readLine();
                        //直到读取当多行注释行尾前，没读取一行内容说明注释行加一
                    } while (!temp.matches(regexNodeEnd));
                    //行尾也算注释行，所以此处再加一
                    notesNum++;
                } else if (temp.matches(regexSpace)) {
                    //当前行匹配空行时空行行加一
                    spaceNum++;
                } else if (temp.matches(regexNode)) {
                    //当前行匹配单行注释时注释行加一
                    notesNum++;
                } else{
                    //如果当前行既不是注释行也不是空行那就是代码行
                    codeNum++;
                }
            }
            System.out.println(file.getName()+",代码行/空行/注释行：" + codeNum + "/" + spaceNum + "/" + notesNum);
            str+=(file.getName()+",代码行/空行/注释行：" + codeNum + "/" + spaceNum + "/" + notesNum+"。\n");
            File outFile = new File(this.outFile);
            bw = new BufferedWriter(new FileWriter(outFile,true));
            bw.write(file.getName()+",代码行/空行/注释行：" + codeNum + "/" + spaceNum + "/" + notesNum+"。\r\n");
            bw.close();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("文件不存在");
        }
        return str;
    }
    /**
     * 递归计算符合要求文件需要计算的数据的计算方法
     * @param fileName 需要计算的含通配符文件
     */
    private void recursiveFiles(String fileName){
        File dir = new File(".");
        //获取当前目录下的所有符合条件的文件
        List<File> files = getFile(dir);
        for (File file : files) {
            if (this.isC) {
                charNum(file);
            }
            if (this.isW) {
                wordNum(file);
            }
            if (this.isL) {
                lineNum(file);
            }
            if (this.isA) {
                complexStat(file);
            }
        }
    }
    public List<File> getFile(File dir) {
        List<File> files = new ArrayList<>();
        File[] subs = dir.listFiles();
        for (File file : subs) {
            //如果this.fileName == null,说明是高级功能GUI在调用，否则为扩展功能调用
            if(file.isFile() && this.fileName == null){
                files.add(file);
            }
            else if (file.isFile() && file.getName().endsWith(this.fileName.substring(1))) {
                //这里我只处理了*在最前的状态，如*.c,*wc.txt,对于hel*.c的情况处理不了，算法有待优化
                files.add(file);
            } else if (file.isDirectory()) {
                files.addAll(getFile(file));
            }
        }
        return files;
    }
    /**
     *  命令行解析完成后，运行次方法执行相应操作
     */
    public void run() {
        if (this.isX){
            //如果this.isX为true,说明命令行使用了-x参数，直接调用GUI
            new WcGUI().showUI();
        }
        if (this.isS){
            recursiveFiles(this.fileName);
        }else {
            if (this.isC) {
                charNum(new File(this.fileName));
            }
            if (this.isW) {
                wordNum(new File(this.fileName));
            }
            if (this.isL) {
                lineNum(new File(this.fileName));
            }
            if (this.isA) {
                complexStat(new File(this.fileName));
            }
        }
    }


    public boolean isC() {
        return isC;
    }

    public void setC(boolean c) {
        isC = c;
    }

    public boolean isW() {
        return isW;
    }

    public void setW(boolean w) {
        isW = w;
    }

    public boolean isL() {
        return isL;
    }

    public void setL(boolean l) {
        isL = l;
    }

    public boolean isS() {
        return isS;
    }

    public void setS(boolean s) {
        isS = s;
    }

    public boolean isA() {
        return isA;
    }

    public void setA(boolean a) {
        isA = a;
    }

    public boolean isE() {
        return isE;
    }

    public void setE(boolean e) {
        isE = e;
    }

    public boolean isX() {
        return isX;
    }

    public void setX(boolean x) {
        isX = x;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOutFile() {
        return outFile;
    }

    public void setOutFile(String outFile) {
        this.outFile = outFile;
    }

    public String getStopList() {
        return stopList;
    }

    public void setStopList(String stopList) {
        this.stopList = stopList;
    }

    public int getCharNum() {
        return charNum;
    }

    public void setCharNum(int charNum) {
        this.charNum = charNum;
    }

    public int getWordNum() {
        return wordNum;
    }

    public void setWordNum(int wordNum) {
        this.wordNum = wordNum;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }

    public int getCodeNum() {
        return codeNum;
    }

    public void setCodeNum(int codeNum) {
        this.codeNum = codeNum;
    }

    public int getNotesNum() {
        return notesNum;
    }

    public void setNotesNum(int notesNum) {
        this.notesNum = notesNum;
    }

    public int getSpaceNum() {
        return spaceNum;
    }

    public void setSpaceNum(int spaceNum) {
        this.spaceNum = spaceNum;
    }
}